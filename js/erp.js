$(document).ready(function () {
   $('.change_status').change(function () {
       let value, rowId;
       value = this.value;
       rowId = $('option:selected', this).attr('data-row-id');
       $.ajax({
           type: "POST",
           url: "UpdateStatus.php",
           data: { row_id: rowId, option_value: value},
           cache: false,
           success: function(response){
               console.log(response);
                window.location.reload();
           }
       });
   });
});