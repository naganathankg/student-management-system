var hrs = 0, min = 0, sec = 0;
var duration;
//Start timer
$("#timerEntry-form").on('submit', function (e) {
    e.preventDefault();
    
    if ($("#timerEntry-form")) {
        let description = $("#timer-description").val();
        let projectId = $("#timer-chooseProject").val();
        let currentUser = $("#current_user").val();
        let id = $("#timerEntry-id").val();
        if (id == "" && description != "" && projectId != "" && currentUser != "") {
            $.ajax({
                type: 'POST',
                url: 'ManageEntries/start.php',
                data: {description: description, projectId: projectId, currentUser: currentUser},
                success: function (response) {
                    console.log(response);

                    $('#timer-controls').removeClass('hidden');
                    $('#entry-mode').addClass('hidden');
                    duration = setInterval(function () {
                        timer();
                    }, 1000);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }
});

//Stop timer

$("#timerEntry-form").on('click', '#stop-timer', function (event) {
    let description = $("#timer-description").val();
    let projectId = $("#timer-chooseProject").val();
    let currentUser = $("#current_user").val();
    if (description && projectId && currentUser) {
        $('#timer-controls').addClass('hidden');
        $('#entry-mode').removeClass('hidden');
        $.ajax({
            type: 'POST',
            url: 'ManageEntries/stop.php',
            data: {description: description, projectId: projectId, currentUser:currentUser},
            success: function (response) {
                let responseArray = JSON.parse(response)
                timeEntryDetails(responseArray);
            }
        });
        event.preventDefault();
        clearInterval(duration);
        $("#timerEntry-id").val("");
        $("#timer").removeAttr('title');
        hrs = 0;
        min = 0;
        sec = 0;
        $("#timer").empty();
        $("#timer").append("00:00:00");
        $("#timer-description").val("")
        $("#timer-chooseProject").val("");
        //$("#timer-chooseProjectTask").html("<option value=''>-- Select Task --</option>");
    } else {
        alert("Please check the fields!");
    }
});


function timer() {
    sec++;
    $("#timer").empty();
    if (sec >= 60) {
        min++;
        sec = 0;
    }

    if (min >= 60) {
        hrs++;
        min = 0;
    }
    var h = ("0" + hrs).slice(-2);
    var m = ("0" + min).slice(-2);
    var s = ("0" + sec).slice(-2);
    let time = h + ":" + m + ":" + s;
    $("#timer").append(time);
}

$(document).ready(function() {
    let currentUser = $("#current_user").val();
    if(currentUser) {
        $.ajax({
            type: 'POST',
            url: 'ManageEntries/onload.php',
            data: {currentUser:currentUser},
            success: function (response) {
                //console.log(JSON.parse(response));
                let responseArray = JSON.parse(response)
                timeEntryDetails(responseArray);
            }
        });
        $.ajax({
            type: 'POST',
            url: 'ManageEntries/timer.php',
            data: {currentUser:currentUser},
            success: function (response) {
                console.log(response);
                let responseArray = JSON.parse(response);
                if(responseArray['is_running']) {
                    $("#entry-mode").addClass('hidden');
                    $("#timer-controls").removeClass('hidden');
                    $("#timer-chooseProject").val(responseArray['project']);
                    $("#timer-description").val(responseArray['description']);
                    hrs = responseArray['hrs'];
                    min = responseArray['min'];
                    sec = responseArray['sec'];
                    duration = setInterval(function () {
                        timer();
                    }, 1000);
                }

                /*let responseArray = JSON.parse(response)
                timeEntryDetails(responseArray);*/
            }
        });
    } else {
        alert("Your session out please logout and login again");
    }

});

function timeEntryDetails(response) {
    $("#time_entries").empty();
    console.log(response);
    let markup = "<table>" +
        "<tr>${project}</tr> " +
        "<tr>${description}</tr> " +
        "<tr>${formatted_start_time}</tr> " +
        "<tr>${formatted_end_time}</tr> " +
        "<tr>${duration}</tr> " +
        "</table>"
    $.template( "timEntryTemplate", markup );
    $.tmpl( "timEntryTemplate", response )
        .appendTo( "#time_entries" );
}