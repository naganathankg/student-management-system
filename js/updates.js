function dynamicSubChanger(sem, sub) {
    var sem = document.getElementById(sem);
    var sub = document.getElementById(sub);

    sub.innerHTML = "";

    if (sem.value == "semester 1" ) {
        var optionArr = ["--select subject--","General tamil 1", "General english 1", "Programming in c","Digital principles and computer organization","Allied mathematical foundation","Programming in c lab","Value education"];
    } else if (sem.value == "semester 2" ) {
        var optionArr = ["--select subject--","General tamil 2","General english 2","Object oriented programming with c++","Operating system","Operations research","Object oriented programming with c++ lab","Linux lab","Introduction to computer networks","Environmental studies"];
    } else if (sem.value == "semester 3" ) {
        var optionArr = ["--select subject--","General tamil 3","General english 3","Cryptography","Data Structures","Allied statistical methods","Network security lab using java","Java programmimg","Python programming lab","Non major elective Pc hardware lab"];
    } else if (sem.value == "semester 4" ) {
        var optionArr = ["--select subject--","General tamil 4","General englishg 4","Relational database management system","penetration testing fundamnetals","Penetration testing lab","Database security lab","Data analytics lab using python","Non major elective basic 2D animation practical"];
    } else if (sem.value == "semester 5" ) {
        var optionArr = ["--select subject--","Cyber forensics","Web technology lab","PHP lab","Software engineering","Cyber forensics lab","Cloud computing","Placement recruitment training"];
    }
    for (var option in optionArr) {
        var pair = optionArr[option];
        var newOp = document.createElement("option");

        newOp.value = pair;
        newOp.innerHTML = pair;
        sub.options.add(newOp);
    }
}
function dynamicSemChanger(year, sem) {
    var year = document.getElementById(year);
    var sem = document.getElementById(sem);

    sem.innerHTML = "";

    if (year.value == "1st year" ) {
        var optionArr = ["--select semester--","semester 1", "semester 2"];
    }
    else if (year.value == "2nd year") {
        var optionArr = ["--select semester--","semester 3", "semester 4"];
    }
    else if (year.value == "3rd year") {
        var optionArr = ["--select semester--","semester 5"];
    }
    for (var option in optionArr) {
        var pos = optionArr[option];
        var newOp = document.createElement("option");

        newOp.value = pos;
        newOp.innerHTML = pos;
        sem.options.add(newOp);
    }
}
function dynamicYear(dept, yearr, selectedValue) {
    var dept = document.getElementById(dept);
    var yearr = document.getElementById(yearr);

    yearr.innerHTML = "";

    if (dept.value == "cs" ) {
        var optionArr = ["--select year--","1st year", "2nd year","3rd year"];
    }
    for (var option in optionArr) {
        var pos = optionArr[option];
        var newOp = document.createElement("option");
        if(pos == selectedValue) {
            newOp.selected = true;
        }
        newOp.value = pos;
        newOp.innerHTML = pos;
        yearr.options.add(newOp);
    }
}