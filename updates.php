<?php
require_once 'dp_connection.php';

session_start();
if(!isset($_SESSION['user'])) {
    $redirectUrl = "http://".$_SERVER['SERVER_NAME'].'/COLLEGE/login.php';
    header('Location:'.$redirectUrl);
}

error_reporting(0);
$id = $_GET['id'];

$workEntryGetQuery = 'SELECT * FROM work_entry WHERE id ='."'$id'";
$result = $conn->query($workEntryGetQuery);
$row = $result->fetch_assoc();

?>
<!DOCTYPE html>

<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>ERP | page</title>
  <link rel="icon" href="erp.ico">
  <link rel="stylesheet" href="work.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script type="text/javascript">

  </script>
</head>

<body style="  background-image: url('comp.jpg'); background-size: cover;">
  <div>
    <div id="myDIV">
    <ul>
      <img src="Naga Ram-logos.jpeg" alt="">
      <h4>NAGARAM - ERP</h4>
    <li><a class="active">Work</a></li>
    <li><a href="attendance.php" >Attendence</a></li>
    <li><a href="marklist.php" >Marklist</a></li>
    <li><a>About</a></li>
     </ul>
    </div>
    <!-- Work -->

    <div class="main-sec">
      <div class="title">
        <h1 align="center" style="position:relative;top:-.3em;">Work sheet</h1>
      </div>
      <form  action = "" method="POST" class="main-element">

          <table>
            <tr>
              <td>
                <label style="font-weight:bold;position:absolute;left:3.5em;top:1em;">Department</label> <br>
                <select id="dept" name="Department" onchange ="dynamicYear(this.id,'year', <?= $row['Department'] ?>)" required>
                  <option value="<?php echo "$dep" ?>" value="Select your Department">Select your departmnet</option>
                  <option <?php if($row['Department'] == 'English') {echo"selected";}  ?> value="English">English</option>
                  <option <?php if($row['Department'] == 'Tamil') {echo"selected";}  ?> value="Tamil">Tamil</option>
                  <option <?php if($row['Department'] == 'Maths') {echo"selected";}  ?> value="Tamil"> value="Maths">Maths</option>
                  <option <?php if($row['Department'] == 'cs') {echo"selected";}  ?> value="cs">Computer science</option>
                  <option <?php if($row['Department'] == 'Commerce') {echo"selected";}  ?> value="Commerce">Commerce</option>
                  <option <?php if($row['Department'] == 'Visual communication') {echo"selected";}  ?> value="Visual communication">Visual communication</option>
                  <option <?php if($row['Department'] == 'Chemistry') {echo"selected";}  ?> value="Chemistry">Chemistry</option>
                  <option <?php if($row['Department'] == 'Physics') {echo"selected";}  ?> value="Physics">Physics</option>
                  <option <?php if($row['Department'] == 'Animation') {echo"selected";}  ?> value="Animation">Animation</option>
                </select>

              </td>
              <td>
                <label style="font-weight:bold;position:absolute;left:18.5em;top:1em;">Year</label><br>
                <select  id="year"  name="Year" onchange="dynamicSemChanger(this.id,'sem')" required>
                </select>
              </td>
              <td>
                <label style="font-weight:bold;position:absolute;left:33.5em;top:1em;">Semester</label><br>
                <select id="sem" name="Semester"  onchange="dynamicSubChanger(this.id,'sub')" required>
                </select>

              </td>
            </tr>
            <tr>
                <td>
                  <label style="font-weight:bold;position:absolute;left:3.5em;top:7em;">Subject</label> <br>
                  <select id="sub" name="Subject"  required>
                  </select>

                </td>
                <td>
                  <label style="font-weight:bold;position:absolute;left:18.5em;top:7em;">Period</label><br>
                  <select class="period" name="Period"  required>
                    <option value="">-- Select Period --</option>
                    <option value="1st Period">1st Period</option>
                    <option value="2rd Period">2rd Period</option>
                    <option value="3rd Period">3rd Period </option>
                    <option value="4th Period">4th Period</option>
                    <option value="5th Period">5th Period</option>
                    <option value="6th Period">6th Period</option>
                  </select>
                </td>
                <td>
                  <label style="font-weight:bold;position:absolute;left:33.5em;top:7em;">Topic</label><br>
                 <input type="text" class="topic" name="Topic" value="<?php echo "$top" ?>">
                </td>

              </tr>
              <tr>
                <label style="font-weight:bold;position:absolute;left:3.5em;top:12.5em;">Day order</label> <br>
                <select class="day-or" name="Dayorder"  required>
                  <option value="">-- Select order --</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                </select>
              </td>
              <td>
                <label style="font-weight:bold;position:absolute;left:18.5em;top:12.5em;">Batch</label><br>
                <select class="batch" name="Batch"  required>
                  <option value="">-- Select Batch --</option>
                  <option value="2011-2014">2011-2014</option>
                  <option value="2014-2017">2014-2017</option>
                  <option value="2017-2020">2017-2020</option>
                  <option value="2020-2023">2020-2023</option>
                </select>
              </td>
              <td>
                <label style="font-weight:bold;position:absolute;left:33.5em;top:12.5em;">Class</label><br>
                <select  class="clas"  name="Class"  required>
                  <option value="">-- Select class --</option>
                  <option value="A">A section</option>
                  <option value="B">B section</option>
                </select>

              </tr>
              <tr>
                <td>
                  <label style="font-weight:bold;position:absolute;left:3.5em;top:18em;">Start time</label> <br>
                  <input type="time" name="Starttime" class="st" value="<?php echo "$sta" ?>"  required>
                </td>
                <td>
                  <label style="font-weight:bold;position:absolute;left:18.5em;top:18em;">End time</label><br>
                  <input type="time" name="Endtime" class="et"  required >
                </td>
                <td>
                  <label style="font-weight:bold;position:absolute;left:33.5em;top:18em;">Date</label><br>
                  <input type="date" name="Dates" class="date"  required >
                </td>
              </tr>

              <tr>
                <td>
                  <button type="submit" name="submit" id="btn">Submit</button>
                </td>
                <td>
                  <button type="reset" name="reset" id="btn">Clear</button>
                </td>
              </tr>

          </table>

      </form>
    </div>
</body>
<script src="js/updates.js"></script>
</html>
