<?php
session_start();
if(!isset($_SESSION['user'])) {
    $redirectUrl = "http://".$_SERVER['SERVER_NAME'].'/COLLEGE/login.php';
    header('Location:'.$redirectUrl);
}

?>


<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>ERP | page</title>
  <link rel="icon" href="erp.ico">
  <link rel="stylesheet" href="work.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>

<body style="background-image:url('mark.jpg');background-size:cover;height:100vh;">
  <div>
    <div id="myDIV">
      <ul>
        <img src="Naga Ram-logos.jpeg" alt="">
        <h4>NAGARAM - ERP</h4>
    <li><a href="index.php" >Work</a></li>
    <li><a href="attendance.php"  >Attendence</a></li>
    <li><a class="active" >Marklist</a></li>
    <li><a href="work_status.php">View Work</a></li>
    <li><a id="logout" href="#">Logout</a></li>
     </ul>
    </div>
    <!-- Work -->
    <h1 style="text-align:center;font-size:3.5em;position:relative;bottom:10px;right:6.9em;color:#411530;">Mark sheet</h1>
    <div class="main-sec-mark">
      <form class="main-element-mark" action="redirect1.php">
          <table>
             <tr>
               <td>
                 <label style="font-weight:bold;position:absolute;left:4.9em;top:1em;">Year</label> <br>
                 <select  class="year-att"  name="Year" required>
                   <option value="">-- Select Year --</option>
                   <option value="1">1st year</option>
                   <option value="2">2nd year</option>
                   <option value="3">3rd year</option>
                 </select>
               </td>
               <td>
                 <label style="font-weight:bold;position:absolute;left:4.9em;top:5em;">Semester</label><br>
                 <select class="sem-att" name="sem" required>
                   <option value="">-- Select semester --</option>
                   <option value="">1st Semester</option>
                   <option value="">2rd Semester</option>
                   <option value="">3rd Semester</option>
                   <option value="">4th Semester</option>
                   <option value="">5th Semester</option>
                 </select>
               </td>
               <td>
                 <label style="font-weight:bold;position:absolute;left:4.9em;top:9.5em;">Department</label><br>
                 <select class="dept-att" name="depart" required>
                   <option value="">Select your departmnet</option>
                   <option value="English">English</option>
                   <option value="Tamil">Tamil</option>
                   <option value="Maths">Maths</option>
                   <option value="Computer science">Computer science</option>
                   <option value="Commerce">Commerce</option>
                   <option value="Visual communication">Visual communication</option>
                   <option value="Chemistry">Chemistry</option>
                   <option value="Physics">Physics</option>
                   <option value="Animation">Animation</option>
                 </select>
               </td>
               <td>
                 <label style="font-weight:bold;position:absolute;left:4.9em;top:23.5em;">Batch</label><br>
                 <select class="batch-mark" name="day" required>
                   <option value="">-- Select Batch --</option>
                   <option value="">2011-2014</option>
                   <option value="">2014-2017</option>
                   <option value="">2017-2020</option>
                   <option value="">2020-2023</option>
                 </select>
               </td>
               <td>
                 <label style="font-weight:bold;position:absolute;left:4.9em;top:18.8em;">Class</label> <br>
                 <select  class="clas-att"  name="class" required>
                   <option value="">-- Select class --</option>
                   <option value="a">A section</option>
                   <option value="b">B section</option>
                 </select>
               </td>
               <td>
                 <label style="font-weight:bold;position:absolute;left:5em;top:14em;">Internal</label><br>
                 <select class="int-mark" name="sem" required>
                   <option value="">-- Select Internal --</option>
                   <option value="">1st Internal</option>
                   <option value="">2rd Internal</option>
                   <option value="">3rd Internal</option>
                 </select>
               </td>
             </tr>
              <tr>
                <td>
                  <button type="submit" name="submit" id="btn-mark">Submit</button>
                </td>
                <td>
                  <button type="reset" name="reset" id="btn-mark">Clear</button>
                </td>
              </tr>
          </table>

      </form>
    </div>
</body>
<script src="js/index.js"></script>

</html>
