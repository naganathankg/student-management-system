<!DOCTYPE>
<html>
<head>
<title>Success - message</title>
<link rel ="icon" href ="verified.png">
<link rel ="stylesheet" href ="success.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200&family=Lato:wght@100&family=Open+Sans:wght@300&family=Poppins:wght@300&family=Roboto+Mono:wght@100;600&family=Sansita+Swashed&family=Source+Code+Pro:wght@500&family=Teko:wght@300;400;600&family=Work+Sans:wght@700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
</head>
<body>
<div class="main">
<div class="first">
<img src="check.png" alt="" class="animated fadeIn">
<h4 class="fadeIn">Success</h4>
</div>
<div class="second">
<h3 class="fadeIn">Congratulation , your account has been successfully created.</h3>
</div>
</div>
</body>
</html>
