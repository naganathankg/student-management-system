<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Login Page</title>
  <link rel="icon" href="user.ico">
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100&family=Open+Sans:wght@300&family=Poppins:wght@300&family=Roboto+Mono:wght@100;600&family=Sansita+Swashed&family=Source+Code+Pro:wght@500&family=Teko:wght@300;400;600&display=swap"
    rel="stylesheet">

</head>

<body>

  <div class="login-first">
    <img src="user2.png" alt="">
          <div class="error-message">
          <?php
          session_start();
          if(isset($_SESSION['error_message'])) {
          echo $_SESSION['error_message'];
          unset($_SESSION['error_message']);
          }
          
          session_destroy();
          ?>
      </div>
    <div class="form">
      <h1>Login Page</h1>
      <form name="f1" onsubmit="return validation()"  action="loginpost.php" method="post" id="form">
        <div class="form-control">
          <label for="name" id="username">Username</label>
          <small class="message-1" id="m1"></small><br>
          <input class="uname" name="uname" type="text" placeholder="ex:nagaram" id="user" ontext="return validation()" required><br>
        </div>
        <label for="password" id="password">Password</label>
        <input class="pass" name="password" type="password" placeholder="ex:nagaram@1" id="passw" required><br>
        <small class="message-2"  id="m2"></small><br>
        <input type="checkbox" onclick="myFunction()" class="showpass">
        <button type="submit" name="button">Login</button>
      </form>
      <h5>show password</h5>
      <p>Not a member?</p>
      <a href="register.html">Sign up</a>
    </div>
  </div>
  <div class="text">
    <h2 style="font-family: 'Work Sans', sans-serif;font-weight:300;">Hello,sir</h2>
    <h4 style="font-family: 'Lato', sans-serif;font-weight:100;">Enter your name and <br>password and start your <br> work</h4>
  </div>

  <h6>Nagaram pvt company limited®</h6>
<script src="sample.js"></script>
  
</body>

</html>
