<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Approval Form</title>
    <link rel="stylesheet" href="samplestyle.css">
    <link rel="stylesheet" href="approval_page.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
<?php
require_once 'dp_connection.php';

$sql = 'SELECT * FROM users';
$result = $conn->query($sql);
?>
<div class="reg-2">
    <div class="heading">
        <h1 align="center">Approval Form</h1>
    </div>
    <div class="form">
        <table style="width:100%">
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Phone Number</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            <?php while($row = $result->fetch_assoc()) : ?>
            <tr>
                <?php switch ($row['status']) {
                    case 0:
                        $status = "Pending";
                        break;
                    case 1:
                        $status = 'Approved';
                        break;
                    case 2:
                        $status = 'Deny';
                        break;
                    default:
                        $status = 'Pending';
                        break;
                } ?>
                <?php
                switch ($row['gender']) {
                    case 'm':
                        $gender = "Male";
                        break;
                    case 'f':
                        $gender = 'Female';
                        break;
                    default:
                        $gender = 'Others';
                        break;
                }

                ?>
                <td><?= $row['firstname'] ?></td>
                <td><?= $row['lastname'] ?></td>
                <td><?= $row['email'] ?></td>
                <td><?= $gender ?></td>
                <td><?= $row['pno'] ?></td>
                <td><?= $status ?></td>
                <td>
                    <select class="change_status">
                        <option value="">Please select</option>
                        <option data-row-id="<?= $row['id'] ?>" value="1">Approve</option>
                        <option data-row-id="<?= $row['id'] ?>" value="2">Deny</option>
                    </select>

                </td>

            </tr>
            <?php endwhile; ?>
        </table>
    </div>

</div>
</body>
<script src="js/erp.js"></script>
</html>
