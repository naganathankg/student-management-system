-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2022 at 05:53 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_attendance`
--

-- --------------------------------------------------------

--
-- Table structure for table `attandance_a`
--

CREATE TABLE `attandance_a` (
  `att_id` int(10) NOT NULL,
  `member_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `attendance` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attandance_a`
--

INSERT INTO `attandance_a` (`att_id`, `member_id`, `date`, `attendance`) VALUES
(1, 1, '2022-09-29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attandance_a2`
--

CREATE TABLE `attandance_a2` (
  `att_id` int(10) NOT NULL,
  `member_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `attendance` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attandance_a2`
--

INSERT INTO `attandance_a2` (`att_id`, `member_id`, `date`, `attendance`) VALUES
(1, 1, '2022-09-29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attandance_a3`
--

CREATE TABLE `attandance_a3` (
  `att_id` int(10) NOT NULL,
  `member_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `attendance` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attandance_a3`
--

INSERT INTO `attandance_a3` (`att_id`, `member_id`, `date`, `attendance`) VALUES
(1, 1, '2022-09-29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attandance_b`
--

CREATE TABLE `attandance_b` (
  `att_id` int(10) NOT NULL,
  `member_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `attendance` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attandance_b`
--

INSERT INTO `attandance_b` (`att_id`, `member_id`, `date`, `attendance`) VALUES
(1, 1, '2022-09-29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attandance_b2`
--

CREATE TABLE `attandance_b2` (
  `att_id` int(10) NOT NULL,
  `member_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `attendance` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attandance_b2`
--

INSERT INTO `attandance_b2` (`att_id`, `member_id`, `date`, `attendance`) VALUES
(1, 1, '2022-09-29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attandance_b3`
--

CREATE TABLE `attandance_b3` (
  `att_id` int(10) NOT NULL,
  `member_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `attendance` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attandance_b3`
--

INSERT INTO `attandance_b3` (`att_id`, `member_id`, `date`, `attendance`) VALUES
(1, 1, '2022-09-29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member_a`
--

CREATE TABLE `member_a` (
  `member_id` int(10) NOT NULL,
  `enrolement_no` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member_a`
--

INSERT INTO `member_a` (`member_id`, `enrolement_no`, `name`, `mobile`, `email`) VALUES
(1, 2022, 'nathan', '1234567890', '20220@slcs.edu.in'),
(2, 20202, 'chandru', '7845129631', 'chandru@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `member_a2`
--

CREATE TABLE `member_a2` (
  `member_id` int(10) NOT NULL,
  `enrolement_no` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member_a2`
--

INSERT INTO `member_a2` (`member_id`, `enrolement_no`, `name`, `mobile`, `email`) VALUES
(1, 2022, 'aathi', '121212121', 'aathi@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `member_a3`
--

CREATE TABLE `member_a3` (
  `member_id` int(10) NOT NULL,
  `enrolement_no` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member_a3`
--

INSERT INTO `member_a3` (`member_id`, `enrolement_no`, `name`, `mobile`, `email`) VALUES
(1, 2022, 'badri', '1245786544', 'badri@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `member_b`
--

CREATE TABLE `member_b` (
  `member_id` int(10) NOT NULL,
  `enrolement_no` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member_b`
--

INSERT INTO `member_b` (`member_id`, `enrolement_no`, `name`, `mobile`, `email`) VALUES
(1, 2022, 'rohit', '1234567890', 'rohit@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `member_b2`
--

CREATE TABLE `member_b2` (
  `member_id` int(10) NOT NULL,
  `enrolement_no` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member_b2`
--

INSERT INTO `member_b2` (`member_id`, `enrolement_no`, `name`, `mobile`, `email`) VALUES
(1, 2022, 'ajay', '4578457845', 'ajay@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `member_b3`
--

CREATE TABLE `member_b3` (
  `member_id` int(10) NOT NULL,
  `enrolement_no` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member_b3`
--

INSERT INTO `member_b3` (`member_id`, `enrolement_no`, `name`, `mobile`, `email`) VALUES
(1, 2022, 'visagan', '9632145781', 'visagan@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attandance_a`
--
ALTER TABLE `attandance_a`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `attandance_a2`
--
ALTER TABLE `attandance_a2`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `attandance_a3`
--
ALTER TABLE `attandance_a3`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `attandance_b`
--
ALTER TABLE `attandance_b`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `attandance_b2`
--
ALTER TABLE `attandance_b2`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `attandance_b3`
--
ALTER TABLE `attandance_b3`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `member_a`
--
ALTER TABLE `member_a`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `member_a2`
--
ALTER TABLE `member_a2`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `member_a3`
--
ALTER TABLE `member_a3`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `member_b`
--
ALTER TABLE `member_b`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `member_b2`
--
ALTER TABLE `member_b2`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `member_b3`
--
ALTER TABLE `member_b3`
  ADD PRIMARY KEY (`member_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attandance_a`
--
ALTER TABLE `attandance_a`
  MODIFY `att_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attandance_a2`
--
ALTER TABLE `attandance_a2`
  MODIFY `att_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attandance_a3`
--
ALTER TABLE `attandance_a3`
  MODIFY `att_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attandance_b`
--
ALTER TABLE `attandance_b`
  MODIFY `att_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attandance_b2`
--
ALTER TABLE `attandance_b2`
  MODIFY `att_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attandance_b3`
--
ALTER TABLE `attandance_b3`
  MODIFY `att_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member_a`
--
ALTER TABLE `member_a`
  MODIFY `member_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `member_a2`
--
ALTER TABLE `member_a2`
  MODIFY `member_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member_a3`
--
ALTER TABLE `member_a3`
  MODIFY `member_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member_b`
--
ALTER TABLE `member_b`
  MODIFY `member_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member_b2`
--
ALTER TABLE `member_b2`
  MODIFY `member_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member_b3`
--
ALTER TABLE `member_b3`
  MODIFY `member_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
