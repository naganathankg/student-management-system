<?php
session_start();
if(!isset($_SESSION['user'])) {
    $redirectUrl = "http://".$_SERVER['SERVER_NAME'].'/COLLEGE/login.php';
    header('Location:'.$redirectUrl);
}

?>


<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>ERP | page</title>
  <link rel="icon" href="erp.ico">
  <link rel="stylesheet" href="work.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script type="text/javascript">
      function dynamicSubChanger(sem, sub) {
          var sem = document.getElementById(sem);
          var sub = document.getElementById(sub);

          sub.innerHTML = "";

          if (sem.value == "semester 1" ) {
              var optionArr = ["--select subject--","General tamil 1", "General english 1", "Programming in c","Digital principles and computer organization","Allied mathematical foundation","Programming in c lab","Value education"];
          } else if (sem.value == "semester 2" ) {
              var optionArr = ["--select subject--","General tamil 2","General english 2","Object oriented programming with c++","Operating system","Operations research","Object oriented programming with c++ lab","Linux lab","Introduction to computer networks","Environmental studies"];
          } else if (sem.value == "semester 3" ) {
              var optionArr = ["--select subject--","General tamil 3","General english 3","Cryptography","Data Structures","Allied statistical methods","Network security lab using java","Java programmimg","Python programming lab","Non major elective Pc hardware lab"];
          } else if (sem.value == "semester 4" ) {
              var optionArr = ["--select subject--","General tamil 4","General englishg 4","Relational database management system","penetration testing fundamnetals","Penetration testing lab","Database security lab","Data analytics lab using python","Non major elective basic 2D animation practical"];
          } else if (sem.value == "semester 5" ) {
              var optionArr = ["--select subject--","Cyber forensics","Web technology lab","PHP lab","Software engineering","Cyber forensics lab","Cloud computing","Placement recruitment training"];
          }
          for (var option in optionArr) {
              var pair = optionArr[option];
              var newOp = document.createElement("option");

              newOp.value = pair;
              newOp.innerHTML = pair;
              sub.options.add(newOp);
          }
      }
      function dynamicSemChanger(year, sem) {
          var year = document.getElementById(year);
          var sem = document.getElementById(sem);

          sem.innerHTML = "";

          if (year.value == "1st year" ) {
              var optionArr = ["--select semester--","semester 1", "semester 2"];
          }
          else if (year.value == "2nd year") {
              var optionArr = ["--select semester--","semester 3", "semester 4"];
          }
          else if (year.value == "3rd year") {
              var optionArr = ["--select semester--","semester 5"];
          }
          for (var option in optionArr) {
              var pos = optionArr[option];
              var newOp = document.createElement("option");

              newOp.value = pos;
              newOp.innerHTML = pos;
              sem.options.add(newOp);
          }
      }
      function dynamicYear(dept, yearr) {
          var dept = document.getElementById(dept);
          var yearr = document.getElementById(yearr);

          yearr.innerHTML = "";

          if (dept.value == "cs" ) {
              var optionArr = ["--select year--","1st year", "2nd year","3rd year"];
          }
          for (var option in optionArr) {
              var pos = optionArr[option];
              var newOp = document.createElement("option");

              newOp.value = pos;
              newOp.innerHTML = pos;
              yearr.options.add(newOp);
          }
      }
  </script>
</head>

<body style="  background-image: url('comp.jpg'); background-size: cover;">
  <div>
    <div id="myDIV">
    <ul>
      <img src="Naga Ram-logos.jpeg" alt="">
      <h4>NAGARAM - ERP</h4>
    <li><a class="active">Work</a></li>
    <li><a href="attendance.php" >Attendence</a></li>
    <li><a href="marklist.php" >Marklist</a></li>
    <li><a href="work_status.php">View Work</a></li>
        <li><a id="logout" href="#">Logout</a></li>
     </ul>
    </div>
    <!-- Work -->

    <div class="main-sec">
      <div class="title">
        <h1 align="center" style="position:relative;top:-.3em;">Work sheet</h1>
      </div>
      <form  action = "work.php" method="POST" class="main-element">
          <table>
            <tr>
              <td>
                <label style="font-weight:bold;position:absolute;left:3.5em;top:1em;">Department</label> <br>
                <select id="dept" name="Department" onchange ="dynamicYear(this.id,'year')" required>
                  <option value="0">Select your departmnet</option>
                  <option value="English">English</option>
                  <option value="Tamil">Tamil</option>
                  <option value="Maths">Maths</option>
                  <option value="cs">Computer science</option>
                  <option value="Commerce">Commerce</option>
                  <option value="Visual communication">Visual communication</option>
                  <option value="Chemistry">Chemistry</option>
                  <option value="Physics">Physics</option>
                  <option value="Animation">Animation</option>
                </select>

              </td>
              <td>
                <label style="font-weight:bold;position:absolute;left:18.5em;top:1em;">Year</label><br>
                <select  id="year"  name="Year" onchange="dynamicSemChanger(this.id,'sem')" required>
                </select>
              </td>
              <td>
                <label style="font-weight:bold;position:absolute;left:33.5em;top:1em;">Semester</label><br>
                <select id="sem" name="Semester"  onchange="dynamicSubChanger(this.id,'sub')" required>
                </select>

              </td>
            </tr>
            <tr>
                <td>
                  <label style="font-weight:bold;position:absolute;left:3.5em;top:7em;">Subject</label> <br>
                  <select id="sub" name="Subject"  required>
                  </select>

                </td>
                <td>
                  <label style="font-weight:bold;position:absolute;left:18.5em;top:7em;">Period</label><br>
                  <select class="period" name="Period"  required>
                    <option value="">-- Select Period --</option>
                    <option value="1st Period">1st Period</option>
                    <option value="2rd Period">2rd Period</option>
                    <option value="3rd Period">3rd Period </option>
                    <option value="4th Period">4th Period</option>
                    <option value="5th Period">5th Period</option>
                    <option value="6th Period">6th Period</option>
                  </select>
                </td>
                <td>
                  <label style="font-weight:bold;position:absolute;left:33.5em;top:7em;">Topic</label><br>
                 <input type="text" class="topic" name="Topic" value="" required>
                </td>

              </tr>
              <tr>
                <label style="font-weight:bold;position:absolute;left:3.5em;top:12.5em;">Day order</label> <br>
                <select class="day-or" name="Dayorder"  required>
                  <option value="">-- Select order --</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                </select>
              </td>
              <td>
                <label style="font-weight:bold;position:absolute;left:18.5em;top:12.5em;">Batch</label><br>
                <select class="batch" name="Batch"  required>
                  <option value="">-- Select Batch --</option>
                  <option value="2011-2014">2011-2014</option>
                  <option value="2014-2017">2014-2017</option>
                  <option value="2017-2020">2017-2020</option>
                  <option value="2020-2023">2020-2023</option>
                </select>
              </td>
              <td>
                <label style="font-weight:bold;position:absolute;left:33.5em;top:12.5em;">Class</label><br>
                <select  class="clas"  name="Class"  required>
                  <option value="">-- Select class --</option>
                  <option value="A">A section</option>
                  <option value="B">B section</option>
                </select>

              </tr>
              <tr>
                <td>
                  <label style="font-weight:bold;position:absolute;left:3.5em;top:18em;">Start time</label> <br>
                  <input type="time" name="Starttime" class="st"   required>
                </td>
                <td>
                  <label style="font-weight:bold;position:absolute;left:18.5em;top:18em;">End time</label><br>
                  <input type="time" name="Endtime" class="et"  required >
                </td>
                <td>
                  <label style="font-weight:bold;position:absolute;left:33.5em;top:18em;">Date</label><br>
                  <input type="date" name="Dates" class="date"  required >
                </td>
              </tr>

              <tr>
                <td>
                  <button type="submit" name="submit" id="btn">Submit</button>
                </td>
                <td>
                  <button type="reset" name="reset" id="btn">Clear</button>
                </td>
              </tr>

          </table>

      </form>
    </div>
</body>
<script src="js/index.js"></script>
</html>
