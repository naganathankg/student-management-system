<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Approval Form</title>
    <link rel="stylesheet" href="samplestyle.css">
    <link rel="stylesheet" href="approval_page.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
<?php
require_once 'dp_connection.php';
session_start();
if(!isset($_SESSION['user'])) {
    $redirectUrl = "http://".$_SERVER['SERVER_NAME'].'/COLLEGE/login.php';
    header('Location:'.$redirectUrl);
}
$userId = $_SESSION['id'];
$userEmail = $_SESSION['user'];
$sql = 'SELECT * FROM work_entry WHERE user_id ='."'$userId'";
$result = $conn->query($sql);
?>
<div class="reg-2">
    <div class="heading">
        <h1 style= text align="center">WORK ENTRY SHEET</h1>
    </div>
    <div class="form">
        <table style="width:100%">
            <tr>
                <th>ID</th>
                <th>User Email</th>
                <th>Department</th>
                <th>Year</th>
                <th>Semester</th>
                <th>Subject</th>
                <th>Period</th>
                <th>Topic</th>
                <th>Dayorder</th>
                <th>Batch</th>
                <th>Class</th>
                <th>Starttime</th>
                <th>Endtime</th>
                <th>Date</th>
                <th>Actions</th>


                
            </tr>
            <?php while($row = $result->fetch_assoc()) : ?>
            <tr>
                <td><?= $row['id'] ?></td>
                <td><?= $userEmail ?></td>
                 <td><?= $row['Department'] ?></td>
                <td><?= $row['Year'] ?></td>
                <td><?= $row['Semester'] ?></td>
                <td><?= $row['Subject'] ?></td>
                <td><?= $row['Period'] ?></td>
                <td><?= $row['Topic'] ?></td>
                <td><?= $row['Dayorder'] ?></td>
                <td><?= $row['Batch'] ?></td>
                <td><?= $row['Class'] ?></td>
                <td><?= $row['Starttime'] ?></td>
                <td><?= $row['Endtime'] ?></td>
                <td><?= $row['Dates'] ?></td>
                <td><a href="Delete.php? id=<?php echo $row['id'];?>"
                       data-toggle="tooltip"
                       data-placement="top"
                       title="Delete">Delete<i class="fa fa-edit"aria-hidden="true"></i></a></td>
               

                     <?php endwhile; ?>
            </tr>
        </table>
    </div>

</div>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
</script>
</body>
<script src="js/erp.js"></script>
</html>