<?php

$postData = $_SERVER;
$year = isset($_REQUEST['Year']) ? $_REQUEST['Year'] : '';
$class = isset($_REQUEST['class']) ? $_REQUEST['class'] : '';

if($year && $class) {
    $redirectUrl = "http://".$_SERVER['SERVER_NAME'].'/COLLEGE/'.$year.'_'.$class;
    header('Location:'.$redirectUrl);
} else {
    $redirectUrl = "http://".$_SERVER['SERVER_NAME'].'/COLLEGE/attendance.php';
    header('Location:'.$redirectUrl);

}

$temp = 10;