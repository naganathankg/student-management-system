<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Registration-form</title>
    <link rel="icon" href="reg.ico">
    <link rel="stylesheet" href="styling.css">
  </head>
  <body>
    <div class="reg-1">
      <!-- <img src="contact.png" alt=""> -->
      <div class="heading">
        <h1>Registration Form</h1>
      </div>
      <div class="form">
        <label for="fname" style="font-weight:bold;position:relative;left:2%;">First name</label><br>
        <input type="text" placeholder="first name" id="fname"><br>
        <label for="lname" style="font-weight:bold;position:relative;right:-2%;top:17px;">Last name</label><br>
        <input type="text" placeholder="last name" id="lname"><br>
        <label for="lname" style="font-weight:bold;position:relative;right:-2%;top:40px;">Gender</label><br>
        <select name="" id="gender">
        <option value="Male">Male</option>
        <option value="Female">Female</option>
        <option value="Others">Others</option>
        </select>
        <br>
        <label for="email" style="font-weight:bold;position:relative;right:-2%;top:60px;">Email</label><br>
        <input type="text" placeholder="ex:naga@gmail.com" id="email"><br>
        <label for="pass" style="font-weight:bold;position:relative;right:-2%;top:80px;">Password</label><br>
        <input type="password" placeholder="password" id="password"><br>
        <label for="pno" style="font-weight:bold;position:relative;right:-2%;top:100px;">Phone number</label><br>
        <input type="number" placeholder="phone number" id="Phno"><br>
        <button type="submit" name="button">submit</button>
      </div>

    </div>
  </body>
</html>
